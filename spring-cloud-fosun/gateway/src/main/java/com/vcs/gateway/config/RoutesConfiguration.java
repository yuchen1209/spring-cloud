package com.vcs.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.reactive.HiddenHttpMethodFilter;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.gateway.config
 * @ClassName: RoutesConfiguration
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 路由服务配置
 * @Date: 2020/12/10 14:57
 * @Version: 1.0
 */
@Configuration
public class RoutesConfiguration {
    @Bean
    public HiddenHttpMethodFilter hiddenHttpMethodFilter() {
        return new HiddenHttpMethodFilter() {
            @Override
            public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
                return chain.filter(exchange);
            }
        };
    }
}
