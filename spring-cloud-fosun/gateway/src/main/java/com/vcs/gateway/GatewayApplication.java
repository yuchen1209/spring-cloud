package com.vcs.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Package:   com.vcs.gateway
 * @ClassName: GatewayApplication
 * @Description:  网关
 * @Author:  zhangs
 * @Email:   853632587@qq.com
 * @Date:    2020/12/10 14:15
 * @Version: 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

}
