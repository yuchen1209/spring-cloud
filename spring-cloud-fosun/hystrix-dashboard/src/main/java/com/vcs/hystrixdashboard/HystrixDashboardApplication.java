package com.vcs.hystrixdashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @Package:   com.vcs.hystrixdashboard
 * @ClassName: HystrixDashboardApplication
 * @Description:  Hystrix Dashboard 引导类
 * @Author:  zhangs
 * @Email:   853632587@qq.com
 * @Date:    2020/12/3 16:57
 * @Version: 1.0
 */
@SpringBootApplication
@EnableHystrixDashboard
public class HystrixDashboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardApplication.class, args);
    }

}
