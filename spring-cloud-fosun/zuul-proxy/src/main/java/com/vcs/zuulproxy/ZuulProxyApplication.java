package com.vcs.zuulproxy;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
/**
 * @Package:   com.vcs.zuulproxy
 * @ClassName: ZuulProxyApplication
 * @Description:  Zuul 代理引导类
 * @Author:  zhangs
 * @Email:   853632587@qq.com
 * @Date:    2020/12/3 17:29
 * @Version: 1.0
 */
@EnableZuulProxy
@SpringCloudApplication
public class ZuulProxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZuulProxyApplication.class, args);
    }

}
