package com.vcs.basicserviceprovider.service.impl;

import com.vcs.basicapi.domain.Account;
import com.vcs.basicapi.feign.AccountService;
import com.vcs.basicserviceprovider.Dao.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Package:   com.vcs.basicserviceprovider.service.impl
 * @ClassName: RamAccountServiceImpl
 * @Description:  账户服务接口
 * @Author:  zhangs
 * @Email:   853632587@qq.com
 * @Date:    2020/12/15 14:28
 * @Version: 1.0
 */
@Service("ramAccountServiceImpl")
public class RamAccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveAccount(Account account) {
        return accountMapper.insertAccount(account);
    }

    @Override
    public Account findById(Integer id) {
        return accountMapper.findAccountById(id);
    }

}
