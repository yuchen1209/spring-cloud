package com.vcs.basicserviceprovider.service.impl;

import com.vcs.basicapi.domain.User;
import com.vcs.basicapi.feign.BasicService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceprovider.service
 * @ClassName: RamBasicService
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 内存实现
 * @Date: 2020/12/3 15:21
 * @Version: 1.0
 */
@Service("ramBasicServiceImpl")
public class RamBasicServiceImpl implements BasicService {

    private Map<String, User> result = new ConcurrentHashMap<>();

    @Override
    public boolean saveUser(User user) {
        return result.put(user.getName(), user) == null;
    }

    @Override
    public List<User> findAll() {
        return  new ArrayList(result.values());
    }

}
