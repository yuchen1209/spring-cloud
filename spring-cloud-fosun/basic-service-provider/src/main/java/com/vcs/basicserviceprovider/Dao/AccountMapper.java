package com.vcs.basicserviceprovider.Dao;

import com.vcs.basicapi.domain.Account;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceprovider.Dao
 * @ClassName: AccountMapper
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 账户数据实现类
 * @Date: 2020/12/15 14:23
 * @Version: 1.0
 */
public interface AccountMapper {

    @Select("select * from account where id = #{Id}")
    Account findAccountById(Integer Id);

    @Insert("insert into account (id,username,password,s_id) values (#{Id},#{userName},#{password},#{sId})")
    int insertAccount(Account account);

}
