package com.vcs.basicserviceprovider;

import com.vcs.basicserviceprovider.stream.EsChannel;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableHystrix
@EnableDiscoveryClient // 激活服务发现客户端
@EnableBinding(EsChannel.class)// 激活 Stream Binding 到 EsChannel；也在将注解用在 BasicMessageByKafka
@MapperScan("com.vcs.basicserviceprovider.Dao.**")//mapper扫描
public class BasicServiceProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicServiceProviderApplication.class, args);
    }

}
