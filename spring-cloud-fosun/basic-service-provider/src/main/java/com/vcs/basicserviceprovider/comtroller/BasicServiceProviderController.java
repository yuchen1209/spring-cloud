package com.vcs.basicserviceprovider.comtroller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.vcs.basicapi.domain.Account;
import com.vcs.basicapi.domain.User;
import com.vcs.basicapi.feign.AccountService;
import com.vcs.basicapi.feign.BasicService;
import com.vcs.basicserviceprovider.message.BasicMessageByKafka;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceprovider.BasicServiceProviderController
 * @ClassName: BasicServiceProviderController
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 基础提供方
 * @Date: 2020/12/3 15:25
 * @Version: 1.0
 */
@RestController
public class BasicServiceProviderController implements BasicService,AccountService {

    @Autowired
    @Qualifier("ramBasicServiceImpl") // 实现 Bean ： ramBasicServiceImpl
    private BasicService basicService;
    @Autowired
    @Qualifier("ramAccountServiceImpl") // 实现 Bean ： ramAccountServiceImpl
    private AccountService accountService;
    @Value("${server.port}")
    private String port;

    private final static Random random = new Random();

    // 通过方法继承，URL 映射 ："/user/save"
    @Override
    public boolean saveUser(@RequestBody User user) {
        return basicService.saveUser(user);
    }

    @HystrixCommand(
            commandProperties = { // Command 配置
                    // 设置操作时间为 100 毫秒
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "100")
            },
            fallbackMethod = "fallbackForGetUsers" // 设置 fallback 方法
    )
    // 通过方法继承，URL 映射 ："/user/find/all"
    @Override
    public List<User> findAll() {
        User user=new User();
        user.setPort(port);
        return Arrays.asList(user);//basicService.findAll();
    }

    /**
     * 获取所有用户列表
     *
     * @return
     */
    @HystrixCommand(
            commandProperties = { // Command 配置
                    // 设置操作时间为 100 毫秒
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "100")
            },
            fallbackMethod = "fallbackForGetUsers" // 设置 fallback 方法
    )
    @GetMapping("/user/list")
    public List<User> getUsers() throws InterruptedException {

        long executeTime = random.nextInt(200);

        // 通过休眠来模拟执行时间
        System.out.println("Execute Time : " + executeTime + " ms");

        Thread.sleep(executeTime);

        return basicService.findAll();
    }

    /**
     * {@link #getUsers()} 的 fallback 方法
     *
     * @return 空集合
     */
    public List<User> fallbackForGetUsers() {
        return Collections.emptyList();
    }

    @GetMapping("get/messages")
    public List<Object> getMessages(){
        return BasicMessageByKafka.getMessages();
    }


    @Override
    public int saveAccount(@RequestBody Account account) {
        return accountService.saveAccount(account);
    }

    @Override
    public Account findById(@RequestBody Integer id) {
        return accountService.findById(id);
    }
}
