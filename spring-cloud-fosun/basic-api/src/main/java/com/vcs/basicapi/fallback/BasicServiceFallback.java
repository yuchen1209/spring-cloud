package com.vcs.basicapi.fallback;

import com.vcs.basicapi.domain.User;
import com.vcs.basicapi.feign.BasicService;

import java.util.Collections;
import java.util.List;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicapi.fallback
 * @ClassName: BasicServiceFallback
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: Fallback 实现
 * @Date: 2020/12/3 14:38
 * @Version: 1.0
 */
public class BasicServiceFallback  implements BasicService {

    @Override
    public boolean saveUser(User user) {
        return false;
    }

    @Override
    public List<User> findAll() {
        return Collections.emptyList();
    }
}
