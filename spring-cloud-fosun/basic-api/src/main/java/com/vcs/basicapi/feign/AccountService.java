package com.vcs.basicapi.feign;

import com.vcs.basicapi.domain.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Package:   com.vcs.basicapi.feign
 * @ClassName: AccountService
 * @Description:  No such property: code for class: Script1
 * @Author:  zhangs
 * @Email:   853632587@qq.com
 * @Date:    2020/12/15 14:30
 * @Version: 1.0
 */

@FeignClient(name = "basic-service-provider",contextId ="account")
public interface AccountService {

    /**
     * 保存账号
     * @param account
     * @return
     */
    @PostMapping("/account/save")
    int saveAccount(Account account);

    /**
     * 查询账号
     * @return non-null
     */
    @GetMapping("/account/find/byId")
    Account findById(Integer id);


}
