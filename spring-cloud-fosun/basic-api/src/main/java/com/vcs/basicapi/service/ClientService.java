package com.vcs.basicapi.service;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceclient.service.impl
 * @ClassName: ClientService
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 客户端测试
 * @Date: 2020/12/16 17:19
 * @Version: 1.0
 */
public interface ClientService {

    void saveRollback();

}
