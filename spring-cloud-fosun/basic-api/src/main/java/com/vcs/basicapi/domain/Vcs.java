package com.vcs.basicapi.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicapi.domain
 * @ClassName: vcs
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 会议表
 * @Date: 2020/12/16 17:26
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@Entity
public class Vcs {

    @Column(name="vcs_id")
    private Integer vcsId;
    @Column(name="creater")
    private String creater;
    @Column(name="vcs_title")
    private String vcsTitle;
    @Column(name="vcs_state")
    private Integer vcsState;

}
