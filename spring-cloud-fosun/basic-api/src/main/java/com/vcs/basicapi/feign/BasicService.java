package com.vcs.basicapi.feign;

import com.vcs.basicapi.domain.User;
import com.vcs.basicapi.fallback.BasicServiceFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicapi.feign
 * @ClassName: BasicService
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 基础服务
 * @Date: 2020/12/3 14:21
 * @Version: 1.0
 */

// 利用占位符避免未来整合硬编码 ${basic.service.name}
@FeignClient(name = "basic-service-provider",contextId ="basic",fallback = BasicServiceFallback.class)
public interface BasicService {

    /**
     * 保存用户
     * @param user
     * @return
     */
    @PostMapping("/user/save")
    boolean saveUser(User user);


    /**
     * 查询所有的用户列表
     * @return non-null
     */
    @GetMapping("/user/find/all")
    List<User> findAll();


}
