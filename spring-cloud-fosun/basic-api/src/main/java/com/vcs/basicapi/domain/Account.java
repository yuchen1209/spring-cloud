package com.vcs.basicapi.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicapi.domain
 * @ClassName: Account
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 账户
 * @Date: 2020/12/15 14:06
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@Entity
public class Account {

    //主键
    @Column(name="id")
    private Integer Id;
    //用户名称
    @Column(name="username")
    private String userName;
    //用户密码
    @Column(name="password")
    private String password;
    //角色ID
    @Column(name="s_id")
    private Integer sId;

}
