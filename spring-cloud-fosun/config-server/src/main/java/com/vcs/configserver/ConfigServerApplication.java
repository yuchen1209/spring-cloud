package com.vcs.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @Package:   com.vcs.configserver
 * @ClassName: ConfigServerApplication
 * @Description:  配置服务器应用
 * @Author:  zhangs
 * @Email:   853632587@qq.com
 * @Date:    2020/12/3 17:05
 * @Version: 1.0
 */
@EnableConfigServer
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApplication.class, args);
    }

}
