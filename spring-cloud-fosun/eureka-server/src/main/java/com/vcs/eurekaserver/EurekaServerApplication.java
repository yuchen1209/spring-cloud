package com.vcs.eurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
/**
 * @Package:   com.vcs.eurekaserver
 * @ClassName: EurekaServerApplication
 * @Description:  Eureka服务
 * @Author:  zhangs
 * @Email:   853632587@qq.com
 * @Date:    2020/12/4 11:03
 * @Version: 1.0
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }

}
