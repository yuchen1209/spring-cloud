package com.vcs.basicserviceclient.stream;

import org.springframework.messaging.MessageChannel;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceprovider.stream
 * @ClassName: MqChannel
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 自定义信息渠道
 * @Date: 2020/12/11 13:32
 * @Version: 1.0
 */
public interface MqChannel {

    /**
     * 告警发送消息通道名称
     */
    String MQ_ALARM_OUTPUT = "mq_alarm_output";

    /**
     * 告警发送消息通道
     * @return channel 返回告警信息发送通道
     */
    //@Output(MQ_ALARM_OUTPUT)
    MessageChannel sendMQAlarmMessage();

}
