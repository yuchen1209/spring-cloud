package com.vcs.basicserviceclient.service.impl;

import com.vcs.basicapi.domain.Account;
import com.vcs.basicapi.domain.Vcs;
import com.vcs.basicapi.feign.AccountService;
import com.vcs.basicapi.service.ClientService;
import com.vcs.basicserviceclient.Dao.VcsMapper;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceclient.service.impl
 * @ClassName: ClientService
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 客户端测试
 * @Date: 2020/12/16 17:19
 * @Version: 1.0
 */
@Service
public class ClientServiceImpl  implements ClientService {

    @Autowired
    private VcsMapper vcsMapper;

    @Autowired
    private AccountService accountService;

    @Override
    @GlobalTransactional//全局事务回滚
    public void saveRollback() {
        Vcs v=new Vcs();
        v.setVcsId(100);
        v.setCreater("zhangs");
        v.setVcsState(1);
        v.setVcsTitle("测试");
        vcsMapper.insertVcs(v);
        Account ac=new Account();
        ac.setId(100);
        ac.setPassword("12345");
        ac.setUserName("123");
        ac.setSId(1);
        accountService.saveAccount(ac);
        accountService.saveAccount(ac);
    }

}
