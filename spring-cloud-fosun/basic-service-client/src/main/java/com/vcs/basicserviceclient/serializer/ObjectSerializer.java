package com.vcs.basicserviceclient.serializer;

import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.Map;


/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceclient.serializer
 * @ClassName: ObjectSerializer
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: java序列化协议
 * @Date: 2020/12/8 13:17
 * @Version: 1.0
 */

public class ObjectSerializer implements Serializer<Object> {

    private Logger logger= LoggerFactory.getLogger(ObjectSerializer.class);

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String topic, Object object) {

        logger.info("topic : {} , object : {}",topic, object);

        byte[] dataArray = null;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(object);
            dataArray = outputStream.toByteArray();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return dataArray;

    }

    @Override
    public void close() {

    }

}
