package com.vcs.basicserviceclient.hystrix;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.Collections;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceclient.hystrix
 * @ClassName: BasicRibbonClientHystrixCommand
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 负载客户端熔断命令
 * @Date: 2020/12/3 16:25
 * @Version: 1.0
 */
public class BasicRibbonClientHystrixCommand extends HystrixCommand<Collection>{

    private final String providerServiceName;

    private final RestTemplate restTemplate;

    public BasicRibbonClientHystrixCommand(String providerServiceName, RestTemplate restTemplate) {
        super(HystrixCommandGroupKey.Factory.asKey(
                "Basic-Ribbon-Client"),
                100);
        this.providerServiceName = providerServiceName;
        this.restTemplate = restTemplate;
    }

    /**
     * 主逻辑实现
     *
     * @return
     * @throws Exception
     */
    @Override
    protected Collection run() throws Exception {
        return restTemplate.getForObject("http://" + providerServiceName + "/user/list", Collection.class);
    }

    /**
     * Fallback 实现
     *
     * @return 空集合
     */
    protected Collection getFallback() {
        return Collections.emptyList();
    }


}
