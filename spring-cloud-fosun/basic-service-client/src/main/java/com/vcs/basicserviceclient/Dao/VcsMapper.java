package com.vcs.basicserviceclient.Dao;

import com.vcs.basicapi.domain.Vcs;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceprovider.Dao
 * @ClassName: AccountMapper
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 会议表
 * @Date: 2020/12/16 14:23
 * @Version: 1.0
 */
public interface VcsMapper {

    @Select("select * from vcs where id = #{Id}")
    Vcs findVcsById(Integer Id);

    @Insert("insert into vcs (vcs_id,creater,vcs_title,vcs_state) values (#{vcsId},#{creater},#{vcsTitle},#{vcsState}) ")
    int insertVcs(Vcs vcs);

}
