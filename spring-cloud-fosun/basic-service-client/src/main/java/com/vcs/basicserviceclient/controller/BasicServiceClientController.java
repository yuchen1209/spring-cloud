package com.vcs.basicserviceclient.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vcs.basicapi.domain.Account;
import com.vcs.basicapi.domain.User;
import com.vcs.basicapi.feign.AccountService;
import com.vcs.basicapi.feign.BasicService;
import com.vcs.basicapi.service.ClientService;
import com.vcs.basicserviceclient.stream.MqChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ProjectName: spring-cloud-fosun
 * @Package: com.vcs.basicserviceclient.controller
 * @ClassName: BasicServiceClientController
 * @Author: zhangs
 * @Email: 853632587@qq.com
 * @Description: 客户端基础服务
 * 注意：官方建议 客户端和服务端不要同时实现 Feign 接口
 * 这里的代码只是一个说明，实际情况最好使用组合的方式，而不是继承
 * @Date: 2020/12/3 16:02
 * @Version: 1.0
 */
@RestController
public class BasicServiceClientController {

    @Autowired
    private BasicService basicService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ClientService clientService;

   /* @Autowired
    private MqChannel mqChannel;*/

    @Autowired
    private ObjectMapper objectMapper;

    private final KafkaTemplate<Integer, User> kafkaTemplate;

    public BasicServiceClientController(KafkaTemplate<Integer, User> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    //利用kafka发送消息
    @PostMapping("/user/send/message")
    public boolean saveUserByMessage(@RequestBody User user) {
        ListenableFuture<SendResult<Integer, User>> future = kafkaTemplate.send("es_alarm_topic", user);
        return future.isDone();
    }


   /* //利用rabbitMq发送消息
    @PostMapping("/user/send/rabbitMq")
    public boolean saveUserByRabbitMq(@RequestBody User user) throws JsonProcessingException {
        MessageChannel messageChannel = mqChannel.sendMQAlarmMessage();
        // User 序列化成 JSON
        String payload = objectMapper.writeValueAsString(user);
        GenericMessage<String> message = new GenericMessage<String>(payload);
        // 发送消息
        return messageChannel.send(message);
    }*/


    // 通过方法继承，URL 映射 ："/user/save"
    @PostMapping("/user/save")
    public boolean saveUser(@RequestBody User user) {
        return basicService.saveUser(user);
    }

    // 通过方法继承，URL 映射 ："/user/find/all"
    @GetMapping("/user/find/all")
    public List<User> findAll() {
        return basicService.findAll();
    }


    @PostMapping("/account/save")
    public Integer saveAccount(@RequestBody Account account) {
        return accountService.saveAccount(account);
    }

    @GetMapping("/account/findById")
    public Account findById(Integer id) {
        return accountService.findById(id);
    }

    @GetMapping("/seataRollback")
    public boolean seataRollback() {
        clientService.saveRollback();
        return true;
    }



}
