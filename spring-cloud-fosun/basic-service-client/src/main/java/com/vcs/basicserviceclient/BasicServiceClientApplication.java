package com.vcs.basicserviceclient;

import com.netflix.loadbalancer.IRule;
import com.vcs.basicapi.feign.AccountService;
import com.vcs.basicapi.feign.BasicService;
import com.vcs.basicserviceclient.rule.MyRule;
import com.vcs.basicserviceclient.stream.MqChannel;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RibbonClient("basic-service-provider") //Ribbon单独使用,指定目标应用名称
@EnableCircuitBreaker // 使用服务短路
@EnableFeignClients(clients = {BasicService.class,AccountService.class}) // 申明 UserService 接口作为 Feign Client 调用
@EnableDiscoveryClient // Eureka整合Ribbon，激活服务发现客户端
//@EnableBinding(MqChannel.class)// 激活 Stream Binding 到 MqChannel；
@MapperScan("com.vcs.basicserviceclient.Dao.**")//mapper扫描
public class BasicServiceClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasicServiceClientApplication.class, args);
    }

    /**
     * 将 {@link MyRule} 暴露成 {@link Bean}
     * @return {@link MyRule}
     */
    @Bean
    public IRule myRule() {
        return new MyRule();
    }

    /**
     * 申明 具有负载均衡能力 {@link RestTemplate}
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
